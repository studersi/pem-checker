use pest::error::Error;
use pest::iterators::{Pair, Pairs};
use pest::Parser;
use pest_derive::Parser;

const ERROR_RULE_PREFIX: &str = "error_";

#[derive(Parser)]
#[grammar = "pest/pem_file.pest"] // relative to src
struct Pem;

/// get a flattened list of error nodes
pub fn get_error_nodes<'a>(
    pairs: Pairs<'a, Rule>,
    stack: Vec<Pair<'a, Rule>>,
) -> Vec<(Pair<'a, Rule>, Vec<Pair<'a, Rule>>)> {
    let mut error_nodes: Vec<(Pair<Rule>, Vec<Pair<Rule>>)> = vec![];

    for pair in pairs {
        let mut sub_stack = stack.clone();

        if format!("{:?}", pair.as_rule())
            .to_lowercase()
            .contains(ERROR_RULE_PREFIX)
        {
            error_nodes.push((pair.clone(), sub_stack.clone()));
        } else {
            sub_stack.push(pair.clone());
        }

        let error_nodes_children = get_error_nodes(pair.clone().into_inner(), sub_stack.clone());

        error_nodes.extend(error_nodes_children);
    }

    return error_nodes;
}

/// check an AST for successfully matched error rules
pub fn check_for_errors(pairs: Pairs<Rule>) -> bool {
    for pair in pairs {
        if format!("{:?}", pair.as_rule())
            .to_lowercase()
            .contains(ERROR_RULE_PREFIX)
        {
            return true;
        } else if check_for_errors(pair.into_inner()) {
            return true;
        }
    }

    return false;
}

/// get error information for error rules
pub fn get_help<'a>(rule: Rule) -> Option<&'a str> {
    match rule {
        Rule::error_cr_newline => Some("There is a CR newline ('\\r') that should be replaced by a LF newline ('\\n')."),
        Rule::error_crlf_newline => Some("There is a CRLF newline ('\\r\\n') that should be replaced by a LF newline ('\\n')."),
        Rule::error_eoi_expected_but_not_reached => Some("The end of input should have been reached but there was still more input that should probably not be there."),
        // Rule::error_eoi_unexpected_end_of_input_instead_of_newline => Some("There was no newline at the end of the last line before the."),
        Rule::error_five_dashes_not_enough_dashes => Some("There should be exactly 5 dashes, this rule triggers if there are not enough dashes for the delimiter."),
        Rule::error_five_dashes_too_many_dashes => Some("There should be exactly 5 dashes, this rule triggers if there are too many dashes for the delimiter."),
        Rule::error_incorrect_closing_label => Some("The closing label hast to match the opening label."),
        Rule::error_leading_whitespace => Some("There is leading whitespace at the start of the line that needs to be removed."),
        Rule::error_trailing_whitespace => Some("There is trailing whitespace at the end of the line that needs to be removed."),
        Rule::error_whitespace => Some("There is whitespace here that needs to be removed."),
        _ => None
    }
}

/// get error information for error rules
pub fn get_err_label_text<'a>(rule: Rule) -> String {
    match rule {
        Rule::error_cr_newline => "there is a CR newline here ('\\r')".to_string(),
        Rule::error_crlf_newline => "there is a CRLF newline here ('\\r\\n')".to_string(),
        Rule::error_eoi_expected_but_not_reached => {
            "this input should probably not be here".to_string()
        }
        // Rule::error_eoi_unexpected_end_of_input_instead_of_newline => "the end of the input is here".to_string(),
        Rule::error_five_dashes_not_enough_dashes => "there are not enough dashes here".to_string(),
        Rule::error_five_dashes_too_many_dashes => "there are too many dashes here".to_string(),
        Rule::error_incorrect_closing_label => {
            "the closing label does not match the opening label".to_string()
        }
        Rule::error_leading_whitespace => "there is leading whitespace here".to_string(),
        Rule::error_trailing_whitespace => "there is trailing whitespace here".to_string(),
        Rule::error_whitespace => "there is whitespace here".to_string(),
        _ => format!("the rule {:?} matched here", rule),
    }
}

/// parse an AST from a string
pub fn parse(
    rule: Rule,
    file_contents: &str,
) -> Result<Result<Pairs<Rule>, Pairs<Rule>>, Error<Rule>> {
    let parsed = Pem::parse(rule, file_contents);
    return match parsed {
        Ok(pairs) => match check_for_errors(pairs.clone()) {
            true => Ok(Err(pairs)),
            false => Ok(Ok(pairs)),
        },
        Err(err) => Err(err),
    };
}

#[cfg(test)]
mod tests {
    use crate::parser::{parse, Rule};
    use insta::{assert_debug_snapshot, assert_snapshot};

    macro_rules! check_result {
        ($parsed:ident, $should_parse_successfully:expr, $parse_tree_should_be_error_free:expr) => {
            match $parsed.clone() {
                Ok(parse_result) => {
                    assert_debug_snapshot!(parse_result);
                    assert_eq!($parse_tree_should_be_error_free, parse_result.is_ok()); // ensure the result from parsing is an un-/successful AST
                }
                Err(err) => {
                    assert_debug_snapshot!(err);
                    assert_snapshot!($parsed.clone().err().unwrap());
                }
            }
            assert_eq!($should_parse_successfully, $parsed.is_ok()); // ensure parser ran un-/successfully
        }
    }

    #[test]
    fn pem_file_rfc7468_section_5_1() {
        let file_contents = include_str!("../test/well-formed/rfc7468/rfc7468-section-5.1.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_file_rfc7468_section_5_2() {
        let file_contents = include_str!("../test/well-formed/rfc7468/rfc7468-section-5.2.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_file_rfc7468_section_5_10() {
        let file_contents = include_str!("../test/well-formed/rfc7468/rfc7468-section-5.10.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_file_rfc7468_section_5_11() {
        let file_contents = include_str!("../test/well-formed/rfc7468/rfc7468-section-5.11.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_file_rfc7468_section_5_11_headers() {
        let file_contents =
            include_str!("../test/well-formed/headers/rfc7468-section-5.11-headers.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_certificate_bundle() {
        let file_contents = include_str!(
            "../test/well-formed/certificate-bundles/rfc7468-section-5.1-certificate-bundle.pem"
        );
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_comments() {
        let file_contents =
            include_str!("../test/well-formed/comments/rfc7468-section-5.1-comments.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_bundle_comments() {
        let file_contents = include_str!(
            "../test/well-formed/comments/rfc7468-section-5.1-certificate-bundle-comments.pem"
        );
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_missing_dash_begin_start() {
        let file_contents = include_str!(
            "../test/malformed/dashes/rfc7468-section-5.1-missing-dash-begin-start.pem"
        );
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_missing_and_superfluous_dash_begin_and_end() {
        let file_contents = include_str!("../test/malformed/dashes/rfc7468-section-5.1-missing-and-superfluous-dash-begin-and-end.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_missing_dash_begin_and_end() {
        let file_contents = include_str!(
            "../test/malformed/dashes/rfc7468-section-5.1-missing-dash-begin-and-end.pem"
        );
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_missing_dash_begin_end() {
        let file_contents =
            include_str!("../test/malformed/dashes/rfc7468-section-5.1-missing-dash-begin-end.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_missing_dash_end_start() {
        let file_contents =
            include_str!("../test/malformed/dashes/rfc7468-section-5.1-missing-dash-end-start.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_missing_dash_end_end() {
        let file_contents =
            include_str!("../test/malformed/dashes/rfc7468-section-5.1-missing-dash-end-end.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_superfluous_dash_begin_and_end() {
        let file_contents = include_str!(
            "../test/malformed/dashes/rfc7468-section-5.1-superfluous-dash-begin-and-end.pem"
        );
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_superfluous_dash_begin_start() {
        let file_contents = include_str!(
            "../test/malformed/dashes/rfc7468-section-5.1-superfluous-dash-begin-start.pem"
        );
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_superfluous_dash_begin_end() {
        let file_contents = include_str!(
            "../test/malformed/dashes/rfc7468-section-5.1-superfluous-dash-begin-end.pem"
        );
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_superfluous_dash_end_start() {
        let file_contents = include_str!(
            "../test/malformed/dashes/rfc7468-section-5.1-superfluous-dash-end-start.pem"
        );
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_superfluous_dash_end_end() {
        let file_contents = include_str!(
            "../test/malformed/dashes/rfc7468-section-5.1-superfluous-dash-end-end.pem"
        );
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_newlines_crlf() {
        let file_contents = include_str!("../test/malformed/newlines/rfc7468-section-5.1_crlf.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_newlines_cr() {
        let file_contents = include_str!("../test/malformed/newlines/rfc7468-section-5.1_cr.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_no_newline_at_eof() {
        let file_contents =
            include_str!("../test/malformed/newlines/rfc7468-section-5.1-no-newline-at-eof.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, false, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_label_unopened() {
        let file_contents =
            include_str!("../test/malformed/labels/rfc7468-section-5.1-unopened.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, false, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_label_unclosed() {
        let file_contents =
            include_str!("../test/malformed/labels/rfc7468-section-5.1-unclosed.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, false, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_label_mismatched() {
        let file_contents =
            include_str!("../test/malformed/labels/rfc7468-section-5.1-mismatched.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_tag_space_before() {
        let file_contents =
            include_str!("../test/malformed/tags/rfc7468-section-5.1-space-before.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_tag_space_between() {
        let file_contents =
            include_str!("../test/malformed/tags/rfc7468-section-5.1-space-between.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_tag_space_after() {
        let file_contents =
            include_str!("../test/malformed/tags/rfc7468-section-5.1-space-after.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_leading_space() {
        let file_contents =
            include_str!("../test/malformed/whitespace/rfc7468-section-5.1-leading-space.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_leading_tabs() {
        let file_contents =
            include_str!("../test/malformed/whitespace/rfc7468-section-5.1-leading-tabs.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_trailing_space() {
        let file_contents =
            include_str!("../test/malformed/whitespace/rfc7468-section-5.1-trailing-space.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_file_rfc7468_section_5_1_trailing_tabs() {
        let file_contents =
            include_str!("../test/malformed/whitespace/rfc7468-section-5.1-trailing-tabs.pem");
        let parsed = parse(Rule::pem_file, &file_contents);
        check_result!(parsed, true, false);
    }

    #[test]
    fn pem_certificate_bundle_rfc7468_section_5_1_certificate_bundle() {
        let file_contents = include_str!(
            "../test/well-formed/certificate-bundles/rfc7468-section-5.1-certificate-bundle.pem"
        );
        let parsed = parse(Rule::pem_certificate_bundle, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_certificate_bundle_rfc7468_section_5_1_bundle_comments() {
        let file_contents = include_str!(
            "../test/well-formed/comments/rfc7468-section-5.1-certificate-bundle-comments.pem"
        );
        let parsed = parse(Rule::pem_certificate_bundle, &file_contents);
        check_result!(parsed, true, true);
    }

    #[test]
    fn pem_private_key_rfc7468_section_5_11_eoi() {
        let file_contents = include_str!("../test/malformed/eoi/rfc7468-section-5.11.pem");
        let parsed = parse(Rule::pem_encrypted_private_key_file, &file_contents);
        check_result!(parsed, true, false);
    }
}
