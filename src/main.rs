mod parser;

use crate::parser::{get_err_label_text, get_error_nodes, get_help, parse, Rule};
use ariadne::{ColorGenerator, Label, ReportBuilder};
use clap::{ArgAction, Parser};
use color_eyre::eyre::eyre;
use color_eyre::owo_colors::OwoColorize;
use color_eyre::Report;
use pest::error::InputLocation;
use pest::iterators::Pairs;
use std::io::Read;
use std::ops::Range;
use std::{fs, io};

/// simple program to check PEM files for syntactic correctness
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// the PEM file to check
    #[arg(num_args = 1..)]
    files: Vec<String>,

    /// the parsing rule to check with
    #[arg(long, default_value = "pem-file")]
    rule: ParserRule,

    /// the output style
    #[arg(long, default_value = "ariadne")]
    output_style: OutputStyle,

    /// increase verbosity
    #[arg(short, long, action = ArgAction::Count)]
    verbose: u8,
}

#[derive(clap::ValueEnum, Clone, Debug, PartialEq, PartialOrd)]
enum OutputStyle {
    Ariadne,
    Pest,
    PestAst,
    Eyre,
}

#[derive(clap::ValueEnum, Clone, Debug, PartialEq, PartialOrd)]
enum ParserRule {
    PemFile,
    PemCertificateBundle,
    PemEncryptedPrivateKeyFile,
    PemApacheMachineCertBundle,
    PemDhparamsFile,
}

impl Into<Rule> for ParserRule {
    fn into(self) -> Rule {
        match self {
            ParserRule::PemFile => Rule::pem_file,
            ParserRule::PemCertificateBundle => Rule::pem_certificate_bundle,
            ParserRule::PemEncryptedPrivateKeyFile => Rule::pem_encrypted_private_key_file,
            ParserRule::PemApacheMachineCertBundle => Rule::pem_apache_machine_cert_bundle,
            ParserRule::PemDhparamsFile => Rule::pem_dhparams_file,
        }
    }
}

// fn print_type_of<T>(_: &T) {
//     println!("{}", std::any::type_name::<T>())
// }

fn print_report(verbosity: u8, file: &str, content: &str, rule: &ParserRule, pairs: Pairs<Rule>) {
    if verbosity >= 2 {
        for pair in pairs.clone().flatten().into_iter() {
            // println!("{:?}\n", pair);

            let mut colors = ColorGenerator::new();
            let a = colors.next();
            let info = colors.next();

            let rule_str = format!("{:?}", rule);
            let msg_report = format!("{:?}", pair.as_rule());
            let msg_label = get_err_label_text(pair.as_rule());
            let msg_help = get_help(pair.as_rule());

            let span = pair.clone().as_span();

            let mut content_expanded = content.to_string();
            let content_last_index = content.len().saturating_sub(1);
            let missing_len = span.end().saturating_sub(content_last_index);

            if missing_len > 0 {
                for _i in 0..=missing_len {
                    content_expanded.push_str(" ");
                }
            }

            let mut report: ReportBuilder<(&&str, Range<usize>)> = ariadne::Report::build(
                ariadne::ReportKind::Custom("TRACE", info),
                &file,
                span.start(),
            );
            report = report.with_message(&msg_report).with_label(
                Label::new((&file, span.start()..span.end()))
                    .with_message(&msg_label)
                    .with_color(a),
            );

            match msg_help {
                None => {}
                Some(msg) => report = report.with_help(&msg),
            }

            report
                .with_note(format!(
                    "The rule {:?} matched while parsing the file {} with the rule {}.",
                    pair.as_rule().bold(),
                    &file.bold(),
                    &rule_str.bold(),
                ))
                .finish()
                .eprint((&file, ariadne::Source::from(content_expanded)))
                .unwrap();
        }
    }

    for (pair, stack) in get_error_nodes(pairs.clone(), vec![]) {
        // println!("{:?}\n", pair);
        // println!("{:?}\n", stack);

        let mut colors = ColorGenerator::new();
        let a = colors.next();

        let rule_str = format!("{:?}", rule);
        let msg_label = get_err_label_text(pair.as_rule());
        let msg_help = get_help(pair.as_rule());

        let span = pair.clone().as_span();

        let mut content_expanded = content.to_string();
        let content_last_index = content.len().saturating_sub(1);
        let missing_len = span.end().saturating_sub(content_last_index);

        if missing_len > 0 {
            for _i in 0..=missing_len {
                content_expanded.push_str(" ");
            }
        }

        if verbosity >= 1 {
            for pair in &stack {
                let mut colors = ColorGenerator::new();
                let a = colors.next();
                let info = colors.next();

                let rule_str = format!("{:?}", rule);
                let msg_report = format!("{:?}", pair.as_rule());
                let msg_label = get_err_label_text(pair.as_rule());
                let msg_help = get_help(pair.as_rule());

                let span = pair.clone().as_span();

                let mut content_expanded = content.to_string();
                let content_last_index = content.len().saturating_sub(1);
                let missing_len = span.end().saturating_sub(content_last_index);

                if missing_len > 0 {
                    for _i in 0..=missing_len {
                        content_expanded.push_str(" ");
                    }
                }

                let mut report: ReportBuilder<(&&str, Range<usize>)> = ariadne::Report::build(
                    ariadne::ReportKind::Custom("INFO", info),
                    &file,
                    span.start(),
                );
                report = report.with_message(&msg_report).with_label(
                    Label::new((&file, span.start()..span.end()))
                        .with_message(&msg_label)
                        .with_color(a),
                );

                match msg_help {
                    None => {}
                    Some(msg) => report = report.with_help(&msg),
                }

                report
                    .with_note(format!(
                        "The rule {:?} matched while parsing the file {} with the rule {}.",
                        pair.as_rule().bold(),
                        &file.bold(),
                        &rule_str.bold(),
                    ))
                    .finish()
                    .eprint((&file, ariadne::Source::from(content_expanded)))
                    .unwrap();
            }
        }

        let mut report: ReportBuilder<(&&str, Range<usize>)> =
            ariadne::Report::build(ariadne::ReportKind::Error, &file, span.start());
        report = report
            .with_message(format!(
                "{:?} ({} > {:?})",
                pair.as_rule(),
                &stack
                    .iter()
                    .map(|pair| format!("{:?}", pair.as_rule()))
                    .collect::<Vec<_>>()
                    .join(" > "),
                pair.as_rule()
            ))
            .with_label(
                Label::new((&file, span.start()..span.end()))
                    .with_message(format!("{:?}: {}", pair.as_rule(), &msg_label))
                    .with_color(a),
            );

        if verbosity >= 1 {
            for stack_pair in stack[stack.len().saturating_sub(3)..].iter() {
                report = report.with_label(
                    Label::new((
                        &file,
                        stack_pair.as_span().start()..stack_pair.as_span().end(),
                    ))
                    .with_message(format!("{:?}", &stack_pair.as_rule()))
                    .with_color(colors.next()),
                );
            }
        }

        match msg_help {
            None => {}
            Some(msg) => report = report.with_help(&msg),
        }

        report
            .with_note(format!("The rule {:?} should never match. The file {} does not appear to be formatted properly for the rule {}.",
                               pair.as_rule().bold(), &file.bold(), &rule_str.bold()
            ))
            .finish()
            .eprint((&file, ariadne::Source::from(content_expanded)))
            .unwrap();
    }
}

fn main() -> Result<(), Report> {
    color_eyre::install()?;

    let mut encountered_errors = false;

    let args = Args::parse();
    let verbosity = args.verbose;

    let mut files = args.files;

    if files.len() == 0 {
        files.extend(vec!["-".to_string()]);
    }

    for file in files {
        let file_contents = match file.as_str() {
            "-" => {
                let mut buf = "".to_string();
                io::stdin()
                    .read_to_string(&mut buf)
                    .expect("Could not read from stdin.");
                buf
            }
            _ => fs::read_to_string(&file)
                .expect(format!("Could not open file '{}'", &file).as_str()),
        };

        let parsed = parse(args.rule.clone().into(), &file_contents);
        match parsed {
            Ok(parse_result) => match parse_result {
                Ok(ast) => {
                    print_report(verbosity, &file, &file_contents, &args.rule, ast);
                }
                Err(ast) => {
                    encountered_errors = true;
                    print_report(verbosity, &file, &file_contents, &args.rule, ast);
                }
            },

            Err(err) => {
                encountered_errors = true;

                match args.output_style {
                    OutputStyle::Ariadne => {
                        let mut colors = ColorGenerator::new();
                        let a = colors.next();
                        // let b = colors.next();
                        // let c = colors.next();
                        // let d = colors.next();

                        let location = match err.location {
                            InputLocation::Pos(p) => p,
                            InputLocation::Span((l, p)) => {
                                file_contents
                                    .lines()
                                    .map(|str| str.len())
                                    .collect::<Vec<usize>>()[0..l]
                                    .iter()
                                    .sum::<usize>()
                                    + p
                            }
                        };

                        let report: ariadne::ReportBuilder<(&String, Range<usize>)> =
                            ariadne::Report::build(ariadne::ReportKind::Error, &file, location);

                        let mut content_expanded = file_contents.to_string();
                        let content_last_index = file_contents.len().saturating_sub(1);
                        let missing_len = location.saturating_sub(content_last_index);

                        if missing_len > 0 {
                            for _i in 0..=missing_len {
                                content_expanded.push_str(" ");
                            }
                        }

                        report
                            .with_label(
                                Label::new((&file, location..location+1))
                                    .with_message(err.variant.message())
                                    .with_color(a)
                            )
                            .with_message(err.variant.message())
                            .with_help(format!("The file {} does not appear to be formatted properly for the rule {}.", &file.clone().bold(), format!("{:?}", args.rule.clone().bold())))
                            .finish()
                            .eprint((&file, ariadne::Source::from(content_expanded)))
                            .unwrap();
                    }
                    OutputStyle::Pest => {
                        eprintln!("{}", err.with_path(&file));
                    }
                    OutputStyle::PestAst => {
                        eprintln!("{:#?}", err.with_path(&file));
                    }
                    OutputStyle::Eyre => {
                        eprintln!("{:?}", eyre!("{}", err.with_path(&file)));
                    }
                }
            }
        }
    }

    return match encountered_errors {
        true => Err(eyre!("Parsing not successful.")),
        false => Ok(()),
    };
}
